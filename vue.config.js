// const { defineConfig } = require('@vue/cli-service')
// module.exports = defineConfig({
//   transpileDependencies: true,
// })
module.exports = {
  devServer: {
    host: 'localhost',
    port: 8080,  // 端口号的配置
    open: true,   // 自动打开浏览器
    // proxy: {
    //   '/api': {
    //     target: 'http://shared-bike.herokuapp.com',
    //     changeOrigin: true,
    //     pathRewrite: {
    //       '^/api': '/api'
    //     }
    //   }
    // },
  }
}