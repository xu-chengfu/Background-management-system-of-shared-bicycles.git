import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import { myInformationAPI } from "@/api";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 1. 用来存储登录成功之后，得到的 token
    token: '',
    userInfo: {} // 定义用户信息对象
  },
  getters: {
    name: state => state.userInfo.name, // 姓名
    sex: state => state.userInfo.sex, // 
    photo: state => state.userInfo.photo, // 用户头像
    phone: state => state.userInfo.phone, // 电话
    address: state => state.userInfo.address, // 地址
    cartType: state => state.userInfo.cartType, // 证件类型
    cart: state => state.userInfo.cart, // 
    nationvalue: state => state.userInfo.nationvalue, // 民族
    money: state => state.userInfo.money, // 余额

  },
  mutations: {
    // 2. 更新 token 的 mutation 函数
    updateToken(state, val) {
      state.token = val
      // console.log(state.token)
    },
    // 更新用户的信息
    updateUserInfo(state, val) {
      state.userInfo = val
      console.log(state.userInfo)
    }
  },
  actions: {
    // 定义初始化用户基本信息的 action 函数
    async initUserInfo(store) {
      const { data: res } = await myInformationAPI()
      if (res.status === 0) {
        store.commit('updateUserInfo', res.data)
      }
    }
  },
  modules: {
  },
  // 配置为 vuex 的插件
  plugins: [createPersistedState()]
})
