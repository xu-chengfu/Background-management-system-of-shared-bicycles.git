//封装要发起的请求
import request from '@/utils/request'  //网络的请求导入
import store from '@/store' // 引入store对象

//封装网络请求方法  
//更新用户会员将到期的处理函数
export const usersvip= ({ id, notvip, }) => request({
    url: '/my/usersinof',
    method: "POST",
    data: {
        id, notvip,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//更新用户未支付的处理函数
export const userspay= ({ id, notpay, }) => request({
    url: '/my/usersinof',
    method: "POST",
    data: {
        id, notpay,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//更新用户信息未填完整的处理函数
export const usersinof = ({ id, info, }) => request({
    url: '/my/usersinof',
    method: "POST",
    data: {
        id, info,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//更新在线状态
export const updateonline = ({ id, online }) => request({
    url: '/my/updateonline',
    method: "POST",
    data: {
        id, online
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//删除用户
export const deleteuser = (id) => {
    return request({
        url: '/my/deleteuser',
        method: 'POST',
        data: {
            id
        },
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    })
}
//更换头像
export const updatePhoto = (photo) => {
    return request({
        url: '/my/update/photo',
        method: 'POST',
        data: {
            photo
        },
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    })
}
//更新余额
export const updateMoney = ({ id, money }) => request({
    url: '/my/updateMoney',
    method: "POST",
    data: {
        id, money
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//更新密码
export const updatePassword = ({ oldPwd, newPwd }) => request({
    url: '/my/updatepwd',
    method: "POST",
    data: {
        oldPwd, newPwd
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//所有用户信息
export const userInformation = params => request({
    url: '/api/userinfo',
    params
})
//更新用户信息
export const updateMyInformation = ({ id, cart, nationvalue, cartType, name, sex, phone, address }) => request({
    url: '/my/userinfo',
    // headers: {
    //     Authorization: store.state.token
    // },
    method: "POST",
    data: {
        id, cart, nationvalue, cartType, name, sex, phone, address
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//更新用户余额
export const updateMyInformation2 = ({ id, money }) => request({
    url: '/my/userinfo',
    // headers: {
    //     Authorization: store.state.token
    // },
    method: "POST",
    data: {
        money, id
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//当前用户信息
export const myInformation = () => request({
    url: '/my/userinfo',
    // headers: {
    //     Authorization: store.state.token
    // }
})
//  注册
export const userRegister = ({ username, password, date }) => request({
    url: '/api/reguser',
    method: "POST",
    data: {
        username,
        password,
        date
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//  登录
export const userLogin = ({ username, password,online }) => request({
    url: '/api/login',
    method: "POST",
    data: {
        username,
        password,
        online
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
