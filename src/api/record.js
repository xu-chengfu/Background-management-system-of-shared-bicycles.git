//封装要发起的请求
import request from '@/utils/request'  //网络的请求导入

//删除租借记录
export const deleterecord = (ids) => {
    return request({
        url: '/rd/deleterecord',
        method: 'POST',
        data: {
            ids
        },
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    })
}
//更新支付状态
export const updatepay = ({ ids, paymentStatus }) => request({
    url: '/rd/updatepay',
    method: "POST",
    data: {
        ids,
        paymentStatus,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//更新租借记录
export const updaterecord = ({ ids, rentalType, paymentStatus, endTime, rentalMoney }) => request({
    url: '/rd/updaterecord',
    method: "POST",
    data: {
        ids,
        rentalType,
        paymentStatus,
        endTime,
        rentalMoney,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//所有租借信息
export const getrecord = params => request({
    url: '/rd/getrecord',
    params
})
//添加租借记录
export const addrecord = ({ id, bikeId, name, startTime, rentalType, paymentStatus }) => request({
    url: '/rd/addrecord',
    method: "POST",
    data: {
        id,
        bikeId,
        name,
        startTime,
        rentalType,
        paymentStatus,
        // endTime,
        // rentalMoney,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})