//统一出口
import {
    userInformation, userRegister, userLogin, myInformation, updateMyInformation,
    updatePassword, updateMoney, updatePhoto, deleteuser, updateonline, usersinof, userspay,
    usersvip
} from '@/api/user';
import { bikeData, updatedBike, deletebike, addbike, breakBike, serviceBike } from '@/api/bike';
import { addrecord, getrecord, updaterecord, updatepay, deleterecord } from '@/api/record';
import { addvip, getvip, getvipId, deletevipId, deletevip } from '@/api/vip';

export const userInformationAPI = userInformation //将所有用户信息拿出
export const userRegisterAPI = userRegister //注册
export const userLoginAPI = userLogin //登录
export const updatePhotoAPI = updatePhoto //更换头像
export const deleteuserAPI = deleteuser //删除用户
export const myInformationAPI = myInformation //将当前用户信息拿出
export const updateMyInformationAPI = updateMyInformation //更新用户信息
export const updatePasswordAPI = updatePassword //更新用户密码
export const updateMoneydAPI = updateMoney //更新用户余额
export const updateonlineAPI = updateonline //更新在线状态
export const usersinofAPI = usersinof //更新用户信息未填完整的处理函数
export const userspayAPI = userspay //更新用户信息未支付的处理函数
export const usersvipAPI = usersvip //更新用户信息会员将过期的处理函数

export const bikeDataAPI = bikeData //获取所有单车数据
export const updatedBikeAPI = updatedBike //更改单车数据
export const deletebikeAPI = deletebike //删除单车
export const addbikeAPI = addbike //添加单车
export const breakBikeAPI = breakBike //发现更新故障单车
export const serviceBikeAPI = serviceBike //维修单车


export const addrecordAPI = addrecord //新增租借记录
export const getrecordAPI = getrecord //所有租借记录
export const updaterecordAPI = updaterecord //更新租借数据
export const updatepayAPI = updatepay //更新支付状态
export const deleterecordAPI = deleterecord //删除租借记录

export const addvipAPI = addvip //新增会员
export const getvipAPI = getvip //所有会员
export const getvipIdAPI = getvipId //根据id获取会员数据
export const deletevipIdAPI = deletevipId //删除会员
export const deletevipAPI = deletevip //批量删除会员
















