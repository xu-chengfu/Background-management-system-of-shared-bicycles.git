//封装要发起的请求
import request from '@/utils/request'  //网络的请求导入

//维修单车
export const serviceBike = ({ bikeId, bikeType }) => request({
    url: '/bk/updatebk',
    method: "POST",
    data: {
        bikeId, bikeType
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//发起更新故障单车
export const breakBike = ({ bikeId, breakType, bikepic, bikeType }) => request({
    url: '/bk/updatebk',
    method: "POST",
    data: {
        bikeId, breakType, bikepic, bikeType
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//新增单车
export const addbike = ({ bikeType, lng, lat }) => request({
    url: '/bk/addbike',
    method: "POST",
    data: {
        bikeType, lng, lat
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//批量删除单车
export const deletebike = (bikeId) => {
    return request({
        url: '/bk/deletebike',
        method: 'POST',
        data: {
            bikeId
        },
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    })
}
//更新单车信息
export const updatedBike = ({ bikeId, bikeType, startTime, endTime }) => request({
    url: '/bk/updatebk',
    method: "POST",
    data: {
        bikeId, bikeType, startTime, endTime
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})
//所有单车信息
export const bikeData = params => request({
    url: '/bk/bike',
    params
})