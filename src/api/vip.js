//封装要发起的请求
import request from '@/utils/request'  //网络的请求导入

//批量删除会员
export const deletevip = (idv) => {
    return request({
        url: '/vp/deletevip',
        method: 'POST',
        data: {
            idv
        },
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    })
}
//删除会员
export const deletevipId = (id) => request({
    url: `/vp/deletevipId/${id} `,
})
//根据id获取用户会员信息
export const getvipId = (id) => request({
    url: `/vp/getvipId/${id} `,
})
//所有会员信息
export const getvip = params => request({
    url: '/vp/getvip',
    params
})
//新增会员
export const addvip = ({ id, phone, name, address, cart, cartType, sex, memberEndTime, memberTime, memberType, }) => request({
    url: '/vp/addvip',
    method: "POST",
    data: {
        id,
        phone,
        name,
        address,
        cart,
        cartType,
        sex,
        memberType,
        memberTime,
        memberEndTime,
    },
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
})